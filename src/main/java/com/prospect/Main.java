package com.prospect;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class Main {

    static int CHECK_INTERVAL = 5;   // Check every 5 seconds
    static int NUM_THREADS = 3;      // Number of threads

    // Thread pool to process domains
    static ExecutorService threadPool = Executors.newFixedThreadPool(NUM_THREADS);

    // Executor to run the check and run reports on a service
    static ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

    static List<Domain> domains;

    static void runCheck() {
        domains.forEach(domain -> threadPool.submit(() -> HttpMonitor.getInstance().check(domain)));
    }

    static void runReport() {
        // Clear previously displayed report
        System.out.print("\033[H\033[2J");
        System.out.flush();

        // Display uptime report
        String reportTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
        System.out.printf("Report %s%n", reportTime);
        domains.forEach(System.out::println);
    }

    public static void main(String[] args) {
        // Create Domain list
        domains = Arrays.stream(Constants.DOMAINS)
                .map(Domain::new)
                .collect(Collectors.toList());

        // Schedule check for each domain
        ses.scheduleAtFixedRate(Main::runCheck, 0, CHECK_INTERVAL, TimeUnit.SECONDS);

        // Schedule report
        ses.scheduleAtFixedRate(Main::runReport, 0, CHECK_INTERVAL, TimeUnit.SECONDS);
    }

}
