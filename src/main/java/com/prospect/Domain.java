package com.prospect;

import java.util.function.Function;

public class Domain {

    private final String domain;
    private String ip;
    private boolean http;
    private boolean https;

    public Domain(String domain) {
        this.domain = domain;
        this.ip = "N/A";
    }

    public void checkIp(Function<String, String> ipChecker) {
        ip = ipChecker.apply(domain);
    }

    public void checkHttp(Function<String, Boolean> httpChecker) {
        http = httpChecker.apply(ip);
    }

    public void checkHttps(Function<String, Boolean> httpsChecker) {
        https = httpsChecker.apply(ip);
    }

    @Override
    public String toString() {
        // Log result to console
        return String.format("%20s", domain) +
                String.format("%18s", ip) +
                "\t\tHTTP: " +
                (http ? "UP" : "DOWN!") +
                " | HTTPS: " +
                (https ? "UP" : "DOWN!");
    }

}
