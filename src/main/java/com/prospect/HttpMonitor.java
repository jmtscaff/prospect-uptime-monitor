package com.prospect;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

class HttpMonitor {

    private static HttpMonitor instance;

    public static HttpMonitor getInstance() {
        if (instance == null) {
            instance = new HttpMonitor();
        }

        return instance;
    }

    HttpMonitor() {
    }

    boolean isReachable(String host, int port) {
        // Ping the provided IP and port
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), 1000);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    String getIpAddress(String domain) {
        // Get the domain's IP so we can ping it
        try {
            return InetAddress.getByName(domain).getHostAddress();
        } catch (UnknownHostException e) {
            return "N/A";
        }
    }

    public void check(Domain domainModel) {
        domainModel.checkIp(this::getIpAddress);
        domainModel.checkHttp(ip -> isReachable(ip, 80));
        domainModel.checkHttps(ip -> isReachable(ip, 443));
    }

}
