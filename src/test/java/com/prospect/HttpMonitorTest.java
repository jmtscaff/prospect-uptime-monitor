package com.prospect;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HttpMonitorTest {

    private static final String IP = "127.0.0.1";
    private static final String DOMAIN = "TEST";

    @Spy
    public HttpMonitor httpMonitor;

    private Domain domain;

    @Before
    public void setUp() {
        doReturn(IP).when(httpMonitor).getIpAddress(DOMAIN);
        doReturn(true).when(httpMonitor).isReachable(IP, 80);
        doReturn(true).when(httpMonitor).isReachable(IP, 443);
        domain = new Domain(DOMAIN);
    }

    @Test
    public void givenDomainWhenCheckThenUpdateDomainStatus() {
        httpMonitor.check(domain);

        String actual = domain.toString();

        assertThat(actual,
                equalTo(String.format("%20s", DOMAIN) +
                        String.format("%18s", IP) +
                        "\t\tHTTP: UP | HTTPS: UP"));
    }
}
