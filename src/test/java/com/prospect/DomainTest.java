package com.prospect;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.function.Function;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DomainTest {

    private static final String DOMAIN = "prospect.com";
    private static final String UP = "UP";
    private static final String DOWN = "DOWN!";
    private static final String IP = "127.0.0.1";
    private static final String NOT_AVAILABLE = "N/A";

    @Mock
    private Function<String, String> ipChecker;

    @Mock
    private Function<String, Boolean> httpChecker;

    private Domain domain;

    @Before
    public void setUp() {
        domain = new Domain(DOMAIN);
        when(httpChecker.apply(any())).thenReturn(true);
    }

    @Test
    public void givenIpCheckerWhenCheckIpThenApply() {
        domain.checkIp(ipChecker);
        verify(ipChecker).apply(DOMAIN);
    }

    @Test
    public void givenHttpCheckerWhenCheckHttpThenApply() {
        domain.checkHttp(httpChecker);
        verify(httpChecker).apply(NOT_AVAILABLE);
    }

    @Test
    public void givenHttpCheckerWhenCheckHttpsThenApply() {
        domain.checkHttps(httpChecker);
        verify(httpChecker).apply(NOT_AVAILABLE);
    }

    @Test
    public void givenDownDownStatusWhenToStringThenPrintStatus() {
        when(httpChecker.apply(any())).thenReturn(false).thenReturn(false);
        checkToString(DOWN, DOWN);
    }

    @Test
    public void givenUpDownStatusWhenToStringThenPrintStatus() {
        when(httpChecker.apply(any())).thenReturn(true).thenReturn(false);
        checkToString(UP, DOWN);
    }

    @Test
    public void givenDownUpStatusWhenToStringThenPrintStatus() {
        when(httpChecker.apply(any())).thenReturn(false).thenReturn(true);
        checkToString(DOWN, UP);
    }

    @Test
    public void givenUpUpStatusWhenToStringThenPrintStatus() {
        when(httpChecker.apply(any())).thenReturn(true).thenReturn(true);
        checkToString(UP, UP);
    }

    public void checkToString(String http, String https) {
        when(ipChecker.apply(DOMAIN)).thenReturn(IP);

        domain.checkIp(ipChecker);
        domain.checkHttp(httpChecker);
        domain.checkHttps(httpChecker);

        String actual = domain.toString();

        assertThat(actual,
                equalTo(String.format("%20s", DOMAIN) +
                        String.format("%18s", IP) +
                        "\t\tHTTP: " +
                        http +
                        " | HTTPS: " +
                        https));
    }
}
